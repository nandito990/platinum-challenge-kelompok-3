import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\Harrys\\Katalon Studio\\platinum-challenge-kelompok-3\\Secondhand-App\\Apk\\app-release.apk', 
    false)

Mobile.tap(findTestObject('Object Repository/Page_Beranda(Home)/btn_Akun'), 0)

Mobile.tap(findTestObject('Object Repository/Akun Saya/btn_Masuk'), 0)

Mobile.setText(findTestObject('Negotiation/android.widget.EditText - Masukkan email'), 'challangeplatinum@getnada.com', 
    0)

Mobile.setEncryptedText(findTestObject('Negotiation/android.widget.EditText - Masukkan password'), 'RigbBhfdqODKcAsiUrg+1Q==', 
    0)

Mobile.tap(findTestObject('Negotiation/android.widget.Button - Masuk'), 0)

Mobile.tap(findTestObject('Negotiation/Seller/Daftar Jual Saya'), 0)

Mobile.tap(findTestObject('Negotiation/Seller/btn_Diminati'), 0)

Mobile.tap(findTestObject('Negotiation/Seller/android.widget.TextView - kabel usb'), 0)

Mobile.tap(findTestObject('Negotiation/Seller/android.widget.Button - Tolak'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Negotiation/Seller/android.widget.TextView - kabel lan'), 0)

Mobile.tap(findTestObject('Negotiation/Seller/android.widget.Button - Terima'), 0)

Mobile.pressHome()

