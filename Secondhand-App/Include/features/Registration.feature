Feature: Registration

	Scenario: User melakukan pendaftaran akun tanpa mengisi semua input field yang dibutuhkan
		When User klik button Akun
		Then User klik button Masuk
		Given Delay 3 detik
		Then User klik text button Daftar
		Then User klik button Daftar
		Given Delay 3 detik
		Then User mendapatkan alert bahwa field tidak boleh kosong
		
	Scenario: User melakukan pendaftaran akun menggunakan email yang tidak valid
		When User mengisi field nama dengan "Pukaxa"
		Then User mengisi field email dengan "pukaxa521@" untuk email yang tidak valid
		Then User mengisi field password dengan "RigbBhfdqOBGNlJIWM1ClA=="
		Then User mengisi field nomor hp dengan "085445695220"
		Then User mengisi field kota dengan "Bandung"
		Then User mengisi field alamat dengan "Jalan kaki"
		Then User klik button Daftar
		Given Delay 3 detik
		Then User mendapatkan alert bahwa email tidak valid
		
	Scenario: User melakukan pendaftaran akun menggunakan password yang tidak valid
		Given Mengosongkan field nama
		Given Mengosongkan field email
		Given Mengosongkan field password
		Given Mengosongkan field nomor hp
		Given Mengosongkan field kota
		Given Mengosongkan field alamat
		When User mengisi field nama dengan "Pukaxa"
		Then User mengisi field email dengann "pukaxa7215@getnada.com"
		Then User mengisi field password dengan "tzH6RvlfSTg=" untuk password yang tidak valid
		Then User mengisi field nomor hp dengan "085445695220"
		Then User mengisi field kota dengan "Bandung"
		Then User mengisi field alamat dengan "Jalan kaki"
		Then User klik button Daftar
		Given Delay 3 detik
		Then User mendapatkan alert bahwa password minimal 6 karakter
		
	Scenario: User melakukan pendaftaran akun menggunakan email yang sudah terdaftar
		Given Mengosongkan field nama
		Given Mengosongkan field email
		Given Mengosongkan field password
		Given Mengosongkan field nomor hp
		Given Mengosongkan field kota
		Given Mengosongkan field alamat
		When User mengisi field nama dengan "Pukaxa"
		Then User mengisi field email yang sudah terdaftar "pukaxa721@getnada.com"
		Then User mengisi field password dengan "RigbBhfdqOBGNlJIWM1ClA=="
		Then User mengisi field nomor hp dengan "085445695220"
		Then User mengisi field kota dengan "Bandung"
		Then User mengisi field alamat dengan "Jalan kaki"
		Then User klik button Daftar
		Given Delay 3 detik
	
	Scenario: User melakukan pendaftaran akun dengan mengisi semua input field yang dibutuhkan dengan valid
    Given Mengosongkan field nama
		Given Mengosongkan field email
		Given Mengosongkan field password
		Given Mengosongkan field nomor hp
		Given Mengosongkan field kota
		Given Mengosongkan field alamat
		When User mengisi field nama dengan "Pukaxa"
		Then User mengisi field email dengan valid "random@getnada.com"
		Then User mengisi field password dengan "RigbBhfdqOBGNlJIWM1ClA=="
		Then User mengisi field nomor hp dengan "085445695220"
		Then User mengisi field kota dengan "Bandung"
		Then User mengisi field alamat dengan "Jalan kaki"
		Then User klik button Daftar
		Given Delay 3 detik
		Then User berhasil masuk ke halaman Akun Saya
