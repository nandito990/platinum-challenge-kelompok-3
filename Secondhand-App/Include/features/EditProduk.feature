Feature: Edit Produk

Scenario: User melakukan ubah nama produk dengan mengosongkan input field nama produk
When User tap list daftar jual saya
Then User tap produk jual
Then User mengkosongkan nama produk
Then User tap button perbaharui
Given Delay 3 detik
Then User mendapatkan alert tidak boleh kosong
Then User tap kembali

Scenario: User melakukan ubah harga produk dengan mengosongkan input field harga produk
When User tap produk jual yang sama
Then User mengkosongkan harga produk
Then User tap button perbaharui
Given Delay 3 detik
Then User mendapatkan alert tidak boleh kosong
Then User tap kembali

Scenario: User melakukan ubah lokasi dengan mengosongkan input field lokasi
When User tap produk jual lagi
Then User mengkosongkan field lokasi
Then User tap button perbaharui
Given Delay 3 detik
Then User mendapatkan alert lokasi tidak boleh kosong
Then User tap kembali

Scenario: User melakukan ubah nama produk menggunakan nama produk baru yang valid
When User tap produk jual
Then User update nama produk
Then User update harga produk
Then User update field lokasi
Then User update deskripsi
Then User tap button perbaharui
Given delay 3 detik
Then User mendapatkan verif produk berhasil
