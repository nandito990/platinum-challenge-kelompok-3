Feature: Login

  Scenario: User melakukan login akun dengan menggunakan email dan password yang valid
    When User klik button akun saya
    Then User klik button masuk saya
    Then User tidak menginput email
    Then User tidak menginput password
    Then User klik masuk login
    Given Delay 3 detik
    Then User mendapatkan message email tidak boleh kosong

  Scenario: User melakukan login akun tanpa mengisi semua input field yang dibutuhkan
    When User menginput email yang belom terdaftar dengan "pukaxa01@getnada.com"
    Then User menginput password yang belom terdaftar dengan "12345678"
    Then User klik masuk login
    Given Delay 3 detik
    Then User mendapatkan message email tidak valid

  Scenario: User melakukan login akun menggunakan password yang tidak terdaftar/salah
    When User menginput email "pukaxa521@getnada.com"
    Then User menginput passowrd yang salah "123"
    Then User klik masuk login
    Given Delay 3 detik
    Then User mendapatkan message email tidak valid

  Scenario: User melakukan login akun dengan menggunakan email dan password yang valid
    When User input credential email "pukaxa521@getnada.com"
    Then User input credential password "12345678"
    Then User klik masuk login
    Given Delay 3 detik
