package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Profile {

	@When("klik akun")
	public void klik_akun() {
		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_akun'), 0)
	}

	@When("klik tanda pencil")
	public void klik_tanda_pencil() {
		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_iconPencil'), 0)
	}

	@When("Upload Foto Dari Galeri")
	public void upload_Foto_Dari_Galeri() {
		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_image'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_galeri'), 0)

		Mobile.tap(findTestObject('Page_Profile/foto'), 0)
	}

	@Then("Verify Upload foto")
	public void verify_Upload_foto() {
		Mobile.verifyElementVisible(findTestObject('Page_Profile/verify_editprofile'), 0)
	}

	@When("Ubah nama dengan nama yang valid")
	public void ubah_nama_dengan_nama_yang_valid() {
		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_editNama'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_Profile/btn_ubahNama'), 'Kelompok 3', 0)

		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_simpan'), 0)
	}

	@Then("Verify ubah nama")
	public void verify_ubah_nama() {
		Mobile.verifyElementVisible(findTestObject('Page_Profile/verify_editprofile'), 0)
	}

	@When("Ubah no handphone dengan no handphone yang tidak valid")
	public void ubah_no_handphone_dengan_no_handphone_yang_tidak_valid() {
		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_editNoHp'), 0)
		Mobile.setText(findTestObject('Object Repository/Page_Profile/btn_ubahNoHp'), '1', 0)
		Mobile.tap(findTestObject('Page_Profile/btn_simpan'), 0)
	}

	@Then("Verify ubah no handphone")
	public void verify_ubah_no_handphone() {
		Mobile.verifyElementVisible(findTestObject('Page_Profile/verify_editprofile'), 0)
	}

	@When("Ubah password dengan input semua field dengan kata sandi lama")
	public void ubah_password_dengan_input_semua_field_dengan_kata_sandi_lama() {
		Mobile.tap(findTestObject('Page_Profile/btn_editpassword'), 0)

		Mobile.setText(findTestObject('Page_Profile/field_sandiLama'), '123456789', 0)

		Mobile.setText(findTestObject('Page_Profile/field_sandiBaru'), '123456789', 0)

		Mobile.setText(findTestObject('Page_Profile/field_konfirmasiSandi'), '123456789', 0)

		Mobile.tap(findTestObject('Page_Profile/btn_simpanSandi'), 0)
	}

	@Then("Verify ubah password")
	public void verify_ubah_password() {
		Mobile.verifyElementVisible(findTestObject('Page_Profile/verify_editpassword'), 0)
	}

	@Then("klik panah kembali")
	public void klik_panah_kembali() {
		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_panahback'), 0)
	}

	@Then("Klik Beranda")
	public void klik_Beranda() {
		Mobile.tap(findTestObject('Object Repository/Page_Profile/btn_beranda'), 0)
	}
}