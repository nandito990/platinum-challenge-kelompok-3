package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class SellProduct {
	@Then("User klik button +")
	public void user_klik_button() {
		Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/btn_Plus'), 0)
	}
	
	@Then("User mengisi field nama produk dengan {string}")
	public void user_mengisi_field_nama_produk_dengan(String namaproduk) {
		Mobile.setText(findTestObject('Object Repository/Page_TambahProduk/inputfield_NamaProduk'), 'Patung Motor', 0)
	}
	
	@Then("User mengisi field harga produk dengan {string}")
	public void user_mengisi_field_harga_produk_dengan(String hargaproduk) {
		Mobile.setText(findTestObject('Object Repository/Page_TambahProduk/inputfield_HargaProduk'), '150000', 0)
	}
	
	@Then("User klik field Pilih Kategori")
	public void user_klik_field_Pilih_Kategori() {
		Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/field_PilihKategori'), 0)
	}
	
	@Then("User memilih kategori Elektronik")
	public void user_memilih_kategori_Elektronik() {
		Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/elektronik'), 0)
	}
	
	@Then("User mengisi field lokasi")
	public void user_mengisi_field_lokasi() {
		Mobile.setText(findTestObject('Object Repository/Page_TambahProduk/inputfield_LokasiProduk'), 'Jakarta', 0)
	}
	
	@Then("User mengisi field deskripsi")
	public void user_mengisi_field_deskripsi() {
		Mobile.setText(findTestObject('Object Repository/Page_TambahProduk/inputfield_DeskripsiProduk'), 'Ini adalah patung motor', 0)
	}
	
	@Then("User klik button upload foto produk")
	public void user_klik_button_upload_foto_produk() {
		Mobile.tap(findTestObject('Page_TambahProduk/btn_UploadFotoProduk'), 0)
	}
	
	@Then("User klik Galeri")
	public void user_klik_Galeri() {
		Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/btn_Gallery'), 0)
	}
	
	@Then("User memilih foto dari galeri")
	public void user_memilih_foto_dari_galeri() {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}
	
	@Then("User klik button Terbitkan")
	public void user_klik_button_Terbitkan() {
		Mobile.tap(findTestObject('Page_TambahProduk/btn_Terbitkan'), 0)
	}
	
	@Then("User melihat informasi produk berhasil diterbitkan")
	public void user_melihat_informasi_produk_berhasil_diterbitkan() {
		Mobile.verifyElementVisible(findTestObject('Page_TambahProduk/verify_berhasilditerbitkan'), 0)
	}
}
