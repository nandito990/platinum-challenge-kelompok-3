package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Registration {

	@When("User klik button Akun")
	public void user_klik_button_Akun() {
		Mobile.tap(findTestObject('Object Repository/Page_Beranda(Home)/btn_Akun'), 0)
	}

	@Then("User klik button Masuk")
	public void user_klik_button_Masuk() {
		Mobile.tap(findTestObject('Object Repository/Akun Saya/btn_Masuk'), 0)
	}

	@Given("Delay {int} detik")
	public void delay_detik(Integer int3) {
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User klik text button Daftar")
	public void user_klik_text_button_Daftar() {
		Mobile.tap(findTestObject('Object Repository/Page_Login/btn_Daftar'), 0)
	}

	@Then("User klik button Daftar")
	public void user_klik_button_Daftar() {
		Mobile.tap(findTestObject('Page_Registrasi/btn_daftar_Daftar'), 0)
	}

	@Then("User mendapatkan alert bahwa field tidak boleh kosong")
	public void user_mendapatkan_alert_bahwa_field_tidak_boleh_kosong() {
		Mobile.verifyElementVisible(findTestObject('Page_Registrasi/verify_namatidakbolehkosong'), 0)
	}

	@When("User mengisi field nama dengan {string}")
	public void user_mengisi_field_nama_dengan(String nama) {
		Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_NamaLengkap'), 'Pukaxa', 0)
	}

	@Then("User mengisi field email dengan {string} untuk email yang tidak valid")
	public void user_mengisi_field_email_dengan_untuk_email_yang_tidak_valid(String emailx) {
		Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Email'), 'pukaxa521@', 0)
	}

	@Then("User mengisi field password dengan {string}")
	public void user_mengisi_field_password_dengan(String password) {
		Mobile.setEncryptedText(findTestObject('Object Repository/Page_Registrasi/inputfield_Password'), 'RigbBhfdqOBGNlJIWM1ClA==', 0)
	}

	@Then("User mengisi field nomor hp dengan {string}")
	public void user_mengisi_field_nomor_hp_dengan(String nomorhp) {
		Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_NomorHP'), '085445695220', 0)
	}

	@Then("User mengisi field kota dengan {string}")
	public void user_mengisi_field_kota_dengan(String kota) {
		Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Kota'), 'Bandung', 0)
	}

	@Then("User mengisi field alamat dengan {string}")
	public void user_mengisi_field_alamat_dengan(String alamat) {
		Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Alamat'), 'Jalan Kaki', 0)
	}

	@Then("User mendapatkan alert bahwa email tidak valid")
	public void user_mendapatkan_alert_bahwa_email_tidak_valid() {
		Mobile.verifyElementVisible(findTestObject('Page_Registrasi/verify_emailtidakvalid'), 0)
	}

	@Given("Mengosongkan field nama")
	public void mengosongkan_field_nama() {
		Mobile.clearText(findTestObject('Page_Registrasi/inputfield_NamaLengkap'), 0)
	}

	@Given("Mengosongkan field email")
	public void mengosongkan_field_email() {
		Mobile.clearText(findTestObject('Page_Registrasi/inputfield_Email'), 0)
	}

	@Given("Mengosongkan field password")
	public void mengosongkan_field_password() {
		Mobile.clearText(findTestObject('Page_Registrasi/inputfield_Password'), 0)
	}

	@Given("Mengosongkan field nomor hp")
	public void mengosongkan_field_nomor_hp() {
		Mobile.clearText(findTestObject('Page_Registrasi/inputfield_NomorHP'), 0)
	}

	@Given("Mengosongkan field kota")
	public void mengosongkan_field_kota() {
		Mobile.clearText(findTestObject('Page_Registrasi/inputfield_Kota'), 0)
	}

	@Given("Mengosongkan field alamat")
	public void mengosongkan_field_alamat() {
		Mobile.clearText(findTestObject('Page_Registrasi/inputfield_Alamat'), 0)
	}

	@Then("User mengisi field password dengan {string} untuk password yang tidak valid")
	public void user_mengisi_field_password_dengan_untuk_password_yang_tidak_valid(String passwordx) {
		Mobile.setEncryptedText(findTestObject('Object Repository/Page_Registrasi/inputfield_Password'), 'tzH6RvlfSTg=', 0)
	}

	@Then("User berhasil masuk ke halaman Akun Saya")
	public void user_berhasil_masuk_ke_halaman_Akun_Saya() {
		Mobile.verifyElementVisible(findTestObject('Page_Registrasi/verify_akunsaya'), 0)
	}

	@Then("User mengisi field email dengann {string}")
	public void user_mengisi_field_email_dengann(String emailq) {
		Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Email'), 'pukaxa7215@getnada.com', 0)
	}

	@Then("User mendapatkan alert bahwa password minimal {int} karakter")
	public void user_mendapatkan_alert_bahwa_password_minimal_karakter(Integer int6) {
		Mobile.verifyElementVisible(findTestObject('Page_Registrasi/verify_passwordharus6karakter'), 0)
	}

	@Then("User mengisi field email yang sudah terdaftar {string}")
	public void user_mengisi_field_email_yang_sudah_terdaftar(String emailtdft) {
		Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Email'), 'pukaxa721@getnada.com', 0)
	}

	@Then("User mengisi field email dengan valid {string}")
	public void user_mengisi_field_email_dengan_valid(String emailva) {
		Date email = new Date()

		String emailDoctor = email.format('yyyyMMddHHmmss')

		def email_code = ('aab' + emailDoctor) + '@getnada.com'

		Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Email'), email_code, 0)
	}
}