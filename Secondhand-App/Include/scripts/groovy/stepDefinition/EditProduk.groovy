package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProduk {
	@When("User tap list daftar jual saya")
	public void user_tap_list_daftar_jual_saya() {
		Mobile.tap(findTestObject('Edit Product/tap_DaftarJualSaya'), 0)
	}
	
	@Then("User tap produk jual")
	public void user_tap_produk_jual() {
		Mobile.tap(findTestObject('Edit Product/product_jual'), 0)
	}
	
	@Then("User mengkosongkan nama produk")
	public void user_mengkosongkan_nama_produk() {
		Mobile.setText(findTestObject('Edit Product/Ubah Product Page/field_namaProduk'), '', 0)
	}
	
	@Then("User tap button perbaharui")
	public void user_tap_button_perbaharui() {
		Mobile.tap(findTestObject('Edit Product/Ubah Product Page/btn_perbarui'), 0)
	}
	
	@Then("User mendapatkan alert tidak boleh kosong")
	public void user_mendapatkan_alert_tidak_boleh_kosong() {
		Mobile.verifyElementVisible(findTestObject('Edit Product/alert_tidakbolehkosong'), 0)
	}
	
	@Then("User tap kembali")
	public void user_tap_kembali() {
		Mobile.tap(findTestObject('Edit Product/Ubah Product Page/btn_kembali'), 0)
	}
	
	@When("User tap produk jual yang sama")
	public void user_tap_produk_jual_yang_sama() {
		Mobile.tap(findTestObject('Edit Product/product_jual'), 0)
	}
	
	@Then("User mengkosongkan harga produk")
	public void user_mengkosongkan_harga_produk() {
		Mobile.setText(findTestObject('Edit Product/Ubah Product Page/field_hargaProduk'), '', 0)
	}
	
	@When("User tap produk jual lagi")
	public void user_tap_produk_jual_lagi() {
		Mobile.tap(findTestObject('Edit Product/product_jual'), 0)
	}
	
	@Then("User mengkosongkan field lokasi")
	public void user_mengkosongkan_field_lokasi() {
		Mobile.setText(findTestObject('Edit Product/Ubah Product Page/field_lokasi'), '', 0)
	}
	
	@Then("User mendapatkan alert lokasi tidak boleh kosong")
	public void user_mendapatkan_alert_lokasi_tidak_boleh_kosong() {
		
	}
	
	@When("User tap produk jual")
	public void user_tap_produk_jual() {
		Mobile.tap(findTestObject('Edit Product/product_jual'), 0)
	}
	
	@Then("User update nama produk")
	public void user_update_nama_produk() {
		Mobile.setText(findTestObject('Edit Product/Ubah Product Page/field_namaProduk'), 'update', 0)
	}
	
	@Then("User update harga produk")
	public void user_update_harga_produk() {
		Mobile.setText(findTestObject('Edit Product/Ubah Product Page/field_hargaProduk'), '100000', 0)
	}
	
	@Then("User update field lokasi")
	public void user_update_field_lokasi() {
		Mobile.setText(findTestObject('Edit Product/Ubah Product Page/field_lokasi'), 'bandung', 0)
	}
	
	@Then("User update deskripsi")
	public void user_update_deskripsi() {
		WebUI.setText(findTestObject('Edit Product/Ubah Product Page/field_deksripsi'), 'updatean')
	}
	
	@Given("delay {int} detik")
	public void delay_detik(Integer int1) {
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User mendapatkan verif produk berhasil")
	public void user_mendapatkan_verif_produk_berhasil() {
		Mobile.verifyElementVisible(findTestObject('Edit Product/verify_produkberhasil'), 0)
	}
	
}