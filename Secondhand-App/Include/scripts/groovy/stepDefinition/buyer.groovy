package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class buyer {

	@Then("Klik tombol kolom cari produk")
	public void klik_tombol_kolom_cari_produk() {
		Mobile.tap(findTestObject('Negotiation/Buyer/Positive/kolom Cari di Second Chance'), 0)
	}

	@Then("user ketik produk")
	public void user_ketik_produk() {
		Mobile.setText(findTestObject('Negotiation/Buyer/Positive/kolom ketik produk'), 'kabel', 0)
	}

	@Then("delay")
	public void delay() {
		Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("klik produk kabel lan")
	public void klik_produk_kabel_lan() {
		Mobile.tap(findTestObject('Negotiation/Buyer/Positive/btn kabel lan'), 0)
	}

	@Then("klik nego kabel lan")
	public void klik_nego_kabel_lan() {
		Mobile.tap(findTestObject('Negotiation/Buyer/Positive/nego kabel lan'), 0)
	}

	@Then("input harga nego kabel lan")
	public void input_harga_nego_kabel_lan() {
		Mobile.setText(findTestObject('Negotiation/Buyer/Positive/nominal harga tawar'), '5000', 0)
	}

	@Then("klik tombol kirim")
	public void klik_tombol_kirim() {
		Mobile.tap(findTestObject('Negotiation/Buyer/Positive/Button - Kirim'), 0)
	}

	@Then("kembali ke halaman cari produk")
	public void kembali_ke_halaman_cari_produk() {
		Mobile.pressBack()
	}

	@Then("klik produk kabel usb")
	public void klik_produk_kabel_usb() {
		Mobile.tap(findTestObject('Negotiation/Buyer/Positive/btn kabel usb'), 0)
	}

	@Then("Klik nego kabel usb")
	public void klik_nego_kabel_usb() {
		Mobile.tap(findTestObject('Negotiation/Buyer/Positive/nego kabel usb'), 0)
	}

	@Then("input harga nego kabel usb")
	public void input_harga_nego_kabel_usb() {
		Mobile.setText(findTestObject('Negotiation/Buyer/Positive/nominal harga tawar'), '17000', 0)
	}

	@Then("klik button kirim")
	public void klik_button_kirim() {
		Mobile.tap(findTestObject('Negotiation/Buyer/Positive/Button - Kirim'), 0)
	}

	@Then("kembali ke halaman product")
	public void kembali_ke_halaman_product() {
		Mobile.pressBack()
	}
}