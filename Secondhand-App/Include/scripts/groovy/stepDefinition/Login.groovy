package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Login {
	@When("User klik button akun saya")
	public void user_klik_button_akun_saya() {
		Mobile.startApplication('C:\\Users\\Marco\\Downloads\\app-release.apk', false)
		Mobile.tap(findTestObject('Page_Beranda(Home)/btn_Akun'), 0)
	}

	@Then("User klik button masuk saya")
	public void user_klik_button_masuk_saya() {
		Mobile.tap(findTestObject('Akun Saya/btn_Masuk'), 0)
	}

	@Then("User tidak menginput email")
	public void user_tidak_menginput_email() {
		Mobile.setText(findTestObject('Page_Login/field_email'), '', 0)
	}

	@Then("User tidak menginput password")
	public void user_tidak_menginput_password() {
		Mobile.setText(findTestObject('Page_Login/field_password'), '', 0)
	}

	@Then("User klik masuk login")
	public void user_klik_masuk_login() {
		Mobile.tap(findTestObject('Page_Login/btn_masuk(login)'), 0)
	}

	@Then("User mendapatkan message email tidak boleh kosong")
	public void user_mendapatkan_message_email_tidak_boleh_kosong() {
		Mobile.verifyElementVisible(findTestObject('Page_Login/message_emailTidakBolehKosong'), 0)
	}

	@When("User menginput email yang belom terdaftar dengan {string}")
	public void user_menginput_email_yang_belom_terdaftar_dengan(String string) {
		Mobile.setText(findTestObject('Page_Login/field_email'), 'pusaxa0@gmail.com', 0)
	}

	@Then("User menginput password yang belom terdaftar dengan {string}")
	public void user_menginput_password_yang_belom_terdaftar_dengan(String string) {
		Mobile.setText(findTestObject('Page_Login/field_password'), '12345678', 0)
	}

	@Then("User mendapatkan message email tidak valid")
	public void user_mendapatkan_message_email_tidak_valid() {
		Mobile.verifyElementVisible(findTestObject('Page_Registrasi/verify_emailtidakvalid'), 0)
	}

	@When("User menginput email {string}")
	public void user_menginput_email(String string) {
		Mobile.setText(findTestObject('Page_Login/field_email'), 'pukaxa521@getnada.com', 0)
	}

	@Then("User menginput passowrd yang salah {string}")
	public void user_menginput_passowrd_yang_salah(String string) {
		Mobile.setText(findTestObject('Page_Login/field_password'), '1234567891011', 0)
	}

	@When("User input credential email {string}")
	public void user_input_credential_email(String string) {
		Mobile.setText(findTestObject('Page_Login/field_email'), 'pukaxa521@getnada.com', 0)
	}

	@Then("User input credential password {string}")
	public void user_input_credential_password(String string) {
		Mobile.setText(findTestObject('Page_Login/field_password'), '12345678', 0)
	}
}