<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_profile</name>
   <tag></tag>
   <elementGuidId>15491c01-8946-472d-a721-b048e3cd2659</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Profile' or . = 'Profile')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Profile</value>
      <webElementGuid>3f77a372-4de0-4693-a918-ec7cc9bddb72</webElementGuid>
   </webElementProperties>
</WebElementEntity>
