<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>trigger_Diskon Hingga 60</name>
   <tag></tag>
   <elementGuidId>8a287aff-8288-4391-b17e-8f1010a0fc1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3.p-section-5.mt-2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>6427e43b-7028-4d6f-8e5d-6882c49f05a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>p-section-5 mt-2</value>
      <webElementGuid>95dcd0c5-9366-411e-8b82-b022f95ccb1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Diskon Hingga 60%</value>
      <webElementGuid>f95f6c9d-6ef3-40f1-bfa1-064e8f427a4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;pb-5&quot;]/div[@class=&quot;container section-5 text-center&quot;]/h3[@class=&quot;p-section-5 mt-2&quot;]</value>
      <webElementGuid>ad736ee7-7810-49eb-9da2-224f3f07845e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/h3</value>
      <webElementGuid>d223635c-3f17-4916-add1-1af134c9071f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan Ramadan Banyak Diskon!'])[1]/following::h3[1]</value>
      <webElementGuid>4a8a6317-2499-40f1-8b36-5d0ad8bbb413</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log Out'])[1]/following::h3[1]</value>
      <webElementGuid>332d8a16-9530-4869-bb97-977064188e8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telusuri Kategori'])[1]/preceding::h3[1]</value>
      <webElementGuid>964f7e60-d9de-4c56-a8fb-3232590a5f69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua'])[1]/preceding::h3[2]</value>
      <webElementGuid>9bf89b16-c9c0-4480-b41b-a37a1d8f10f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Diskon Hingga 60%']/parent::*</value>
      <webElementGuid>0cf50129-72ae-431b-b8c8-60d0638a9e18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>ca4e3c35-9a3c-438f-b15e-a2486c151af9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Diskon Hingga 60%' or . = 'Diskon Hingga 60%')]</value>
      <webElementGuid>30e2e53d-1128-4ff9-82cf-2965bc863495</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
