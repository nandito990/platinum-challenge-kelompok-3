package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Profile {

	@When("Klik tombol user")
	public void klik_tombol_user() {
		WebUI.click(findTestObject('Page_Profile/btn_user'))
	}

	@When("Klik tombol Profile")
	public void klik_tombol_Profile() {
		WebUI.click(findTestObject('Page_Profile/btn_profile'))
	}

	@When("User melakukan submit tanpa mengisi atau merubah value pada semua field")
	public void user_melakukan_submit_tanpa_mengisi_atau_merubah_value_pada_semua_field() {
	}

	@Then("klik submit lagi ya")
	public void klik_submit_lagi_ya() {
		WebUI.click(findTestObject('Page_Profile/btn_submit'))
	}

	@Then("Verify berhasil")
	public void verify_berhasil() {
		WebUI.verifyElementVisible(findTestObject('Page_Profile/verify_berhasil'))

		WebUI.takeScreenshot()

		WebUI.click(findTestObject('Page_Profile/btn_closeberhasil'))
	}


	@When("User melakukan ubah nama menggunakan nama baru yang tidak valid")
	public void user_melakukan_ubah_nama_menggunakan_nama_baru_yang_tidak_valid() {
		WebUI.setText(findTestObject('Page_Profile/field_nama'), '-')
	}

	@Then("klik submit lagi juga")
	public void klik_submit_lagi_juga() {
		WebUI.click(findTestObject('Page_Profile/btn_submit'))
	}

	@Then("Verify berhasil ya")
	public void verify_berhasil_ya() {
		WebUI.verifyElementVisible(findTestObject('Page_Profile/verify_berhasil'))
		WebUI.takeScreenshot()

		WebUI.click(findTestObject('Page_Profile/btn_closeberhasil'))
	}

	@When("User melakukan ubah nomor hp menggunakan nomor hp baru yang tidak valid")
	public void user_melakukan_ubah_nomor_hp_menggunakan_nomor_hp_baru_yang_tidak_valid() {
		WebUI.setText(findTestObject('Page_Profile/field_nohandphone'), 'satu')
	}

	@Then("klik submit lagi")
	public void klik_submit_lagi() {
		WebUI.click(findTestObject('Page_Profile/btn_submit'))
	}

	@Then("Verify berhasil lagi")
	public void verify_berhasil_lagi() {
		WebUI.verifyElementVisible(findTestObject('Page_Profile/verify_berhasil'))
		WebUI.takeScreenshot()

		WebUI.click(findTestObject('Page_Profile/btn_closeberhasil'))
	}

	@When("Mengisi semua input field yang dibutuhkan dengan valid")
	public void mengisi_semua_input_field_yang_dibutuhkan_dengan_valid() {
		WebUI.setText(findTestObject('Page_Profile/field_nama'), 'Kelompok 3')

		WebUI.click(findTestObject('Page_Profile/field_kota'), FailureHandling.STOP_ON_FAILURE)

		WebUI.selectOptionByValue(findTestObject('Page_Profile/btn_pasuruan'), 'Bogor', false)

		WebUI.setText(findTestObject('Page_Profile/field_alamat'), 'Jalan utama')

		WebUI.setText(findTestObject('Page_Profile/field_nohandphone'), '08123456789')
	}

	@Then("klik submit juga lagi")
	public void klik_submit_juga_lagi() {
		WebUI.click(findTestObject('Page_Profile/btn_submit'))
	}

	@Then("Verify berhasil juga")
	public void verify_berhasil_juga() {
		WebUI.verifyElementVisible(findTestObject('Page_Profile/verify_berhasil'))
		WebUI.takeScreenshot()
		WebUI.click(findTestObject('Page_Profile/btn_closeberhasil'))
	}

	@Then("kembali ke Home page")
	public void kembali_ke_Home_page() {
		WebUI.click(findTestObject('Page_Profile/btn_homepage'))
	}
}
