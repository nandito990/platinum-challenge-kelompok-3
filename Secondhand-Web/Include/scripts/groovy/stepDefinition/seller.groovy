package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class seller {

	@Then("klik list sale")
	public void klik_list_sale() {
		WebUI.click(findTestObject('Negotiation/Seller_negotiation_agreement/btn_list_sale'))
	}

	@Then("Klik tombol diminati")
	public void klik_tombol_diminati() {
		WebUI.click(findTestObject('Negotiation/Seller_negotiation_agreement/btn_diminati'))
	}

	@Then("pilih product yang diminati")
	public void pilih_product_yang_diminati() {
		WebUI.click(findTestObject('Negotiation/Seller_negotiation_agreement/user_buyers'))
	}

	@Then("Klik ditolak")
	public void klik_ditolak() {
		WebUI.click(findTestObject('Negotiation/Seller_negotiation_agreement/button_Tolak'))
	}

	@Then("Klik button Iya")
	public void klik_button_Iya() {
		WebUI.click(findTestObject('Negotiation/Seller_negotiation_agreement/button_Iya'))
	}

	@Then("kembali ke semua product")
	public void kembali_ke_semua_product() {
		WebUI.click(findTestObject('Negotiation/Seller_negotiation_agreement/btn_back_diminati'))
	}

	@Then("klik tombol user")
	public void klik_tombol_user() {
		WebUI.click(findTestObject('Negotiation/Seller_negotiation_agreement/user_buyers'))
	}

	@Then("Tekan tombol terima")
	public void tekan_tombol_terima() {
		WebUI.click(findTestObject('Negotiation/Seller_negotiation_agreement/button_Terima'))
	}
	@Then("Klik ulangi button user")
	public void klik_ulangi_button_user() {
		WebUI.click(findTestObject('Page_Logout/btn_user'))
	}

	@Then("Klik ulangi logout")
	public void klik_ulangi_logout() {
		WebUI.click(findTestObject('Page_Logout/btn_logout'))
	}

	@Then("Verifikasi kembali tombol logout")
	public void verifikasi_kembali_tombol_logout() {
		WebUI.verifyElementVisible(findTestObject('Page_Logout/verify_logout'))
	}
}