package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class Category {
	@When("User scroll ke Telusuri Kategori")
	public void user_scroll_ke_Telusuri_Kategori() {
		WebUI.scrollToElement(findTestObject('Fitur_Kategori/trigger_Diskon Hingga 60'), 3)
	}

	@Then("User klik kategori Hoby")
	public void user_klik_kategori_Hoby() {
		WebUI.click(findTestObject('Fitur_Kategori/btn_Hoby'))
	}

	@Then("User melihat barang-barang pada kategori Hoby")
	public void user_melihat_barang_barang_pada_kategori_Hoby() {
		WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Hoby'), FailureHandling.STOP_ON_FAILURE)
	}

	@Given("Delay {double} detik")
	public void delay_detik(Double double1) {
		WebUI.delay(1.5)
	}

	@Then("User klik kategori Elektronik")
	public void user_klik_kategori_Elektronik() {
		WebUI.click(findTestObject('Fitur_Kategori/btn_Elektronik'))
	}

	@Then("User melihat barang-barang pada kategori Elektronik")
	public void user_melihat_barang_barang_pada_kategori_Elektronik() {
		WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Elektronik'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User klik kategori Kendaraan")
	public void user_klik_kategori_Kendaraan() {
		WebUI.click(findTestObject('Fitur_Kategori/btn_Kendaraan'))
	}

	@Then("User melihat barang-barang pada kategori Kendaraan")
	public void user_melihat_barang_barang_pada_kategori_Kendaraan() {
		WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Kendaraan'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User klik kategori Baju")
	public void user_klik_kategori_Baju() {
		WebUI.click(findTestObject('Fitur_Kategori/btn_Baju'))
	}

	@Then("User melihat barang-barang pada kategori Baju")
	public void user_melihat_barang_barang_pada_kategori_Baju() {
		WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Baju'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User klik kategori Kesehatan")
	public void user_klik_kategori_Kesehatan() {
		WebUI.click(findTestObject('Fitur_Kategori/btn_Kesehatan'))
	}

	@Then("User melihat barang-barang pada kategori Kesehatan")
	public void user_melihat_barang_barang_pada_kategori_Kesehatan() {
		WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Kesehatan'), FailureHandling.STOP_ON_FAILURE)
	}
}
