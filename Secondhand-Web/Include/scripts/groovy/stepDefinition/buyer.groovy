package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



public class buyer {


	@Then("click category baju")
	public void click_category_baju() {
		WebUI.click(findTestObject('Fitur_Kategori/btn_Baju'))
	}

	@Then("scroll button jersey MU")
	public void scroll_button_jersey_MU() {
		WebUI.scrollToElement(findTestObject('Negotiation/buyer_negotiation/title_emyu'), 0)
	}

	@Then("Delay sepuluh detik")
	public void delay_sepuluh_detik() {
		WebUI.delay(10)
	}

	@Then("click button jersey MU")
	public void click_button_jersey_MU() {
		WebUI.click(findTestObject('Negotiation/buyer_negotiation/title_emyu'))
	}

	@Then("scroll tittel jersey")
	public void scroll_tittel_jersey() {
		WebUI.scrollToElement(findTestObject('Negotiation/buyer_negotiation/jersey_emyu'), 0)
	}

	@Then("Delay lima detik")
	public void delay_lima_detik() {
		WebUI.delay(5)
	}

	@Then("Click button saya tertarik dan nego")
	public void click_button_saya_tertarik_dan_nego() {
		WebUI.click(findTestObject('Negotiation/buyer_negotiation/button_Saya tertarik dan ingin nego'))
	}

	@Then("insert harga tawar")
	public void insert_harga_tawar() {
		WebUI.setText(findTestObject('Negotiation/buyer_negotiation/form_harga_tawar'), '_+_%^\';[].,/?')
	}

	@Then("Click button kirim")
	public void click_button_kirim() {
		WebUI.click(findTestObject('Negotiation/buyer_negotiation/button_Kirim'))
	}
	@Then("Clear Harga Tawar")
	public void clear_Harga_Tawar() {
		WebUI.clearText(findTestObject('Negotiation/buyer_negotiation/form_harga_tawar'))
	}

	@Then("insert harga tawar lagi")
	public void insert_harga_tawar_lagi() {
		WebUI.setText(findTestObject('Negotiation/buyer_negotiation/form_harga_tawar'), '400000')
	}

	@Then("click tombol kirim")
	public void click_tombol_kirim() {
		WebUI.click(findTestObject('Negotiation/buyer_negotiation/button_Kirim'))
	}

	@When("klik button logo")
	public void klik_button_logo() {
		WebUI.click(findTestObject('Negotiation/buyer_negotiation/btn_logo'))
	}

	@When("click kategori baju")
	public void click_kategori_baju() {
		WebUI.click(findTestObject('Fitur_Kategori/btn_Baju'))
	}

	@Then("scroll jersey juve")
	public void scroll_jersey_juve() {
		WebUI.scrollToElement(findTestObject('Negotiation/buyer_negotiation/jersey_juventus'), 0)
	}

	@Then("klik jersey juve")
	public void klik_jersey_juve() {
		WebUI.click(findTestObject('Negotiation/buyer_negotiation/jersey_juventus'))
	}

	@Then("scroll jersey juve lagi")
	public void scroll_jersey_juve_lagi() {
		WebUI.scrollToElement(findTestObject('Negotiation/buyer_negotiation/judul_jersey_juve'), 0)
	}

	@Then("click tawar harga")
	public void click_tawar_harga() {
		WebUI.click(findTestObject('Negotiation/buyer_negotiation/button_Saya tertarik dan ingin nego'))
	}

	@Then("input tawar harga")
	public void input_tawar_harga() {
		WebUI.setText(findTestObject('Negotiation/buyer_negotiation/form_harga_tawar'), '200000')
	}

	@Then("Klik Kirim")
	public void klik_Kirim() {
		WebUI.click(findTestObject('Negotiation/buyer_negotiation/button_Kirim'))
	}

	@Then("Click icon user")
	public void click_icon_user() {
		WebUI.click(findTestObject('Page_Logout/btn_user'))
	}

	@Then("click tombol logout")
	public void click_tombol_logout() {
		WebUI.click(findTestObject('Page_Logout/btn_logout'))
	}

	@Then("Verifikasi logout")
	public void verifikasi_logout() {
		WebUI.verifyElementVisible(findTestObject('Page_Logout/verify_logout'))
	}
}