package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProduk {
	@When("klik tombol list")
	public void klik_tombol_list() {
		WebUI.click(findTestObject('Edit Produk/home page/btn_list_daftarjual_home'))
	}
	
	@Then("Klik foto produk")
	public void klik_foto_produk() {
		WebUI.click(findTestObject('Edit Produk/Produk/product_list_edit'))
	}
	
	@Then("Klik button edit produk")
	public void klik_button_edit_produk() {
		WebUI.click(findTestObject('Edit Produk/button_Edit_product'))
	}
	
	@When("User mengkosongkan nama produk")
	public void user_mengkosongkan_nama_produk() {
		WebUI.setText(findTestObject('Edit Produk/field_namaProduk'), '')
	}
	
	@Then("klik terbitkan")
	public void klik_terbitkan() {
		WebUI.click(findTestObject('Edit Produk/button_Terbitkan'))
	}
	
	@When("User mengkosongkan harga produk")
	public void user_mengkosongkan_harga_produk() {
		WebUI.setText(findTestObject('Edit Produk/field_hargaProduct'), '')
	}
	
	@Then("klik terbitkan lagi")
	public void klik_terbitkan_lagi() {
		WebUI.click(findTestObject('Edit Produk/button_Terbitkan'))
	}
	
	@When("User mengkosongkan Deskripsi")
	public void user_mengkosongkan_Deskripsi() {
		WebUI.setText(findTestObject('Edit Produk/textarea_deskripsi'), '')
	}
	
	@Then("klik terbitkan lagi juga")
	public void klik_terbitkan_lagi_juga() {
		WebUI.click(findTestObject('Edit Produk/button_Terbitkan'))
	}
	
	@When("User mengisikan valid edit")
	public void user_mengisikan_valid_edit() {
		WebUI.setText(findTestObject('Edit Produk/field_namaProduk'), 'update')
		
		WebUI.setText(findTestObject('Edit Produk/field_hargaProduct'), '10000')
		
		WebUI.setText(findTestObject('Edit Produk/textarea_deskripsi'), 'update')
		
	}
	
	@Then("klik terbitkan lagi lagi")
	public void klik_terbitkan_lagi_lagi() {
		WebUI.click(findTestObject('Edit Produk/button_Terbitkan'))
	}
}