package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class SellProduct {
	@When("User klik button + Jual")
	public void user_klik_button_Jual() {
		WebUI.click(findTestObject('Page_Beranda(Home)/afterLogin/btn_Jual(TambahProduk)'))
	}

	@Then("User mengisi field nama produk dengan {string}")
	public void user_mengisi_field_nama_produk_dengan(String namaproduk) {
		WebUI.setText(findTestObject('Page_TambahProduk/field_tambahProd_namaProduk'), 'Patung Motor')
	}

	@Then("User mengisi field harga produk dengan {string}")
	public void user_mengisi_field_harga_produk_dengan(String hargaproduk) {
		WebUI.setText(findTestObject('Page_TambahProduk/field_tambahProd_hargaProduk'), '15000')
	}

	@Then("User memilih kategori Hoby")
	public void user_memilih_kategori_Hoby() {
		WebUI.selectOptionByValue(findTestObject('Page_TambahProduk/select_field_Kategori'), '1', true)
	}

	@Then("User mengisi field deskripsi")
	public void user_mengisi_field_deskripsi() {
		WebUI.setText(findTestObject('Page_TambahProduk/field_tambahProd_Deskripsi'), 'Ini adalah patung motor')
	}

//	@Then("User upload foto produk")
//	public void user_upload_foto_produk() {
//		WebUI.uploadFile(findTestObject('Page_TambahProduk/field_tambahprod_UploadFoto'), 'C:\\Users\\nandi\\Katalon Studio\\Platinum Challenge\\Secondhand-Web\\Photo\\1.png')
//	}

	@Then("User klik button Terbitkan")
	public void user_klik_button_Terbitkan() {
		WebUI.click(findTestObject('Page_TambahProduk/btn_Terbitkan'))
	}
}
