package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Login {
//	@When("User tidak mengisi field email login")
//	public void user_tidak_mengisi_field_email_login() {
//		WebUI.setText(findTestObject('Page_Login/field_login_Email'), '')
//	}

//	@Then("User tidak mengisi field password login")
//	public void user_tidak_mengisi_field_password_login() {
//		WebUI.setText(findTestObject('Page_Login/field_login_Password'), '')
//	}

	@Then("User klik tombol masuk")
	public void user_klik_tombol_masuk() {
		WebUI.click(findTestObject('Page_Login/btn_login_Masuk'))
	}

	@When("User mengisi field email login {string}")
	public void user_mengisi_field_email_login(String email_login) {
		WebUI.setText(findTestObject('Page_Login/field_login_Email'), 'challangeplatinum@getnada.com')
	}

	@Given("Menghapus field email login")
	public void menghapus_field_email_login() {
		WebUI.clearText(findTestObject('Page_Login/field_login_Email'))
	}

	@Given("Menghapus field password login")
	public void menghapus_field_password_login() {
		WebUI.clearText(findTestObject('Page_Login/field_login_Password'))
	}

	@Then("User mengisi field password login {string}")
	public void user_mengisi_field_password_login(String password_login) {
		WebUI.setText(findTestObject('Page_Login/field_login_Password'), '123456789')
	}

	@When("User mengisi field email login yang salah {string}")
	public void user_mengisi_field_email_login_yang_salah(String email_login) {
		WebUI.setText(findTestObject('Page_Login/field_login_Email'), 'kelompok3@gmail.com')
	}

	@Then("User mendapatkan error message")
	public void user_mendapatkan_error_message() {
		WebUI.verifyElementVisible(findTestObject('Page_Login/verify_element'))
	}

	@When("User mengisi field email yang belom di verifikasi {string}")
	public void user_mengisi_field_email_yang_belom_di_verifikasi(String email_login) {
		WebUI.setText(findTestObject('Page_Login/field_login_Email'), 'pukaxa501@getnada.com')
	}

	@Then("User mengisi field password email yang belom di verif {string}")
	public void user_mengisi_field_password_email_yang_belom_di_verif(String password_salah) {
		WebUI.setText(findTestObject('Page_Login/field_login_Password'), '12345678')
	}

	@Then("User mendapatkan error message verif")
	public void user_mendapatkan_error_message_verif() {
		WebUI.verifyElementVisible(findTestObject('Zverfikasi_login/verif_email_belum_verifikasi'))
	}

	@Given("User menghapus field email login")
	public void user_menghapus_field_email_login() {
		WebUI.clearText(findTestObject('Page_Login/field_login_Email'))
	}

	@Given("User menghapus field password login")
	public void user_menghapus_field_password_login() {
		WebUI.clearText(findTestObject('Page_Login/field_login_Password'))
	}
}