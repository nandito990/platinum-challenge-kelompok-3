package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class Registration {
	@When("User klik button Masuk")
	public void user_klik_button_Masuk() {
		WebUI.click(findTestObject('Page_Beranda(Home)/beforeLogin/btn_Masuk'))
	}

	@Then("User klik text button Daftar di sini")
	public void user_klik_text_button_Daftar_di_sini() {
		WebUI.click(findTestObject('Page_Login/btn_Daftardisini'))
	}

	@Then("User klik button Daftar")
	public void user_klik_button_Daftar() {
		WebUI.click(findTestObject('Page_Daftar/btn_daftar_Daftar'))
	}

	@When("User mengisi field nama dengan {string}")
	public void user_mengisi_field_nama_dengan(String nama) {
		WebUI.setText(findTestObject('Page_Daftar/field_daftar_Nama'), 'Pukaxa')
	}

	@Then("User mengisi field email dengan {string} untuk email yang tidak valid")
	public void user_mengisi_field_email_dengan_untuk_email_yang_tidak_valid(String email) {
		WebUI.setText(findTestObject('Page_Daftar/field_daftar_Email'), 'pukaxa500@')
	}

	@Then("User mengisi field password dengan {string}")
	public void user_mengisi_field_password_dengan(String password) {
		WebUI.setEncryptedText(findTestObject('Page_Daftar/field_daftar_Password'), 'RigbBhfdqOBGNlJIWM1ClA==')
	}

	@Given("Mengosongkan field nama")
	public void mengosongkan_field_nama() {
		WebUI.clearText(findTestObject('Page_Daftar/field_daftar_Nama'))
	}

	@Given("Mengosongkan field email")
	public void mengosongkan_field_email() {
		WebUI.clearText(findTestObject('Page_Daftar/field_daftar_Email'))
	}

	@Given("Mengosongkan field password")
	public void mengosongkan_field_password() {
		WebUI.clearText(findTestObject('Page_Daftar/field_daftar_Password'))
	}

	@Then("User mengisi field email dengan {string}")
	public void user_mengisi_field_email_dengan(String email) {
		WebUI.setText(findTestObject('Page_Daftar/field_daftar_Email'), 'pukaxa500@getnada.com')
	}

	@Then("User mengisi field email {string}")
	public void user_mengisi_field_email(String email2) {
		Date email = new Date()

		String emailDoctor = email.format('yyyyMMddHHmmss')

		def email_code = ('aab' + emailDoctor) + '@getnada.com'

		WebUI.setText(findTestObject('Page_Daftar/field_daftar_Email'), email_code)
	}

	@Given("Delay {int} detik")
	public void delay_detik(Integer int1) {
		WebUI.delay(3)
	}

	@Then("User screenshot layar")
	public void user_screenshot_layar() {
		WebUI.takeScreenshot()
	}

	@Then("User mendapatkan info email sudah digunakan")
	public void user_mendapatkan_info_email_sudah_digunakan() {
		WebUI.verifyElementVisible(findTestObject('zVerifikasi_Registrasi/verif_emailSudahDigunakan'))
	}

	@Then("User mendapatkan info untuk verifikasi email")
	public void user_mendapatkan_info_untuk_verifikasi_email() {
		WebUI.verifyElementVisible(findTestObject('zVerifikasi_Registrasi/verif_silahkanVerifikasiEmail'))
	}
}
