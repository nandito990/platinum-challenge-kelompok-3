Feature: SellProduct
	Sebagai User, saya ingin menambah produk jualan saya pada web Second Hand Store

	Scenario: User melakukan tambah produk dengan mengisi semua input field yang dibutuhkan dengan valid
		When User klik button + Jual
		Then User mengisi field nama produk dengan "Patung Motor"
		Then User mengisi field harga produk dengan "15000"
		Then User memilih kategori Hoby
		Then User mengisi field deskripsi
		Then User klik button Terbitkan
		Then User screenshot layar