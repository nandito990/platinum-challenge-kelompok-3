Feature: Profile

Scenario: Pengguna ingin dapat berhasil mengubah data profile


When Klik tombol user
And Klik tombol Profile
When User melakukan submit tanpa mengisi atau merubah value pada semua field
Then klik submit lagi ya
And Verify berhasil
When User melakukan ubah nama menggunakan nama baru yang tidak valid
Then klik submit lagi juga
And Verify berhasil ya
When User melakukan ubah nomor hp menggunakan nomor hp baru yang tidak valid
Then klik submit lagi
And Verify berhasil lagi
When Mengisi semua input field yang dibutuhkan dengan valid
Then klik submit juga lagi
And Verify berhasil juga
And kembali ke Home page