Feature: Category
	Sebagai User, saya ingin melakukan pengelompokkan barang berdasarkan kategori pada web Second Hand Store

	Scenario: User melakukan sortir produk dengan memilih kategori yang tersedia
		When User scroll ke Telusuri Kategori
		Then User klik kategori Hoby
		Then User melihat barang-barang pada kategori Hoby
		Given Delay 1.5 detik
		Then User klik kategori Elektronik
		Then User melihat barang-barang pada kategori Elektronik
		Given Delay 1.5 detik
		Then User klik kategori Kendaraan
		Then User melihat barang-barang pada kategori Kendaraan
		Given Delay 1.5 detik
		Then User klik kategori Baju
		Then User melihat barang-barang pada kategori Baju
		Given Delay 1.5 detik
		Then User klik kategori Kesehatan
		Then User melihat barang-barang pada kategori Kesehatan
		Given Delay 1.5 detik
		Then User screenshot layar
