import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Page_Profile/btn_user'))

WebUI.click(findTestObject('Page_Profile/btn_profile'))

WebUI.setText(findTestObject('Page_Profile/field_nama'), 'Kelompok 3')

WebUI.click(findTestObject('Page_Profile/field_kota'), FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByValue(findTestObject('Page_Profile/btn_pasuruan'), 'Bogor', false)

WebUI.setText(findTestObject('Page_Profile/field_alamat'), 'Jalan utama')

WebUI.setText(findTestObject('Page_Profile/field_nohandphone'), '085612345678')

WebUI.click(findTestObject('Page_Profile/btn_submit'))

WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('Page_Profile/verify_berhasil'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_Profile/btn_homepage'))

