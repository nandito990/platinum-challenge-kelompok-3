import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.click(findTestObject('Page_Beranda(Home)/beforeLogin/btn_Masuk'))
//
//WebUI.delay(3)
//
//WebUI.click(findTestObject('Page_Login/btn_Daftardisini'))
WebUI.clearText(findTestObject('Page_Daftar/field_daftar_Nama'))

WebUI.clearText(findTestObject('Page_Daftar/field_daftar_Email'))

WebUI.clearText(findTestObject('Page_Daftar/field_daftar_Password'))

WebUI.setText(findTestObject('Page_Daftar/field_daftar_Nama'), 'Pukaxa b')

Date email = new Date()

String emailDoctor = email.format('yyyyMMddHHmmss')

def email_code = ('aab' + emailDoctor) + '@getnada.com'

WebUI.setText(findTestObject('Page_Daftar/field_daftar_Email'), email_code)

//WebUI.setText(findTestObject('Page_Daftar/field_daftar_Email'), 'pukaxa504@getnada.com')
WebUI.setEncryptedText(findTestObject('Page_Daftar/field_daftar_Password'), 'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.click(findTestObject('Page_Daftar/btn_daftar_Daftar'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('zVerifikasi_Registrasi/verif_silahkanVerifikasiEmail'))

WebUI.takeScreenshot()

