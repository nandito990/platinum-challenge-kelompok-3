import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.scrollToElement(findTestObject('Fitur_Kategori/trigger_Diskon Hingga 60'), 3)

WebUI.click(findTestObject('Fitur_Kategori/btn_Hoby'))

WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Hoby'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1.5)

WebUI.click(findTestObject('Fitur_Kategori/btn_Elektronik'))

WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Elektronik'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1.5)

WebUI.click(findTestObject('Fitur_Kategori/btn_Kendaraan'))

WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Kendaraan'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1.5)

WebUI.click(findTestObject('Fitur_Kategori/btn_Baju'))

WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Baju'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1.5)

WebUI.click(findTestObject('Fitur_Kategori/btn_Kesehatan'))

WebUI.verifyElementVisible(findTestObject('zVerifikasi_Category/verif_category_Kesehatan'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1.5)

WebUI.takeScreenshot()

